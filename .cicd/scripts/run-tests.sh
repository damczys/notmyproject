#!/usr/bin/env bash

COV_FAIL_UNDER=75
PY_VERSION='python3'

cd $1
${PY_VERSION} -m coverage run --branch $1/manage.py test --debug-mode --debug-sql
${PY_VERSION} -m coverage report -m
${PY_VERSION} -m coverage html -d ../report/
${PY_VERSION} -m coverage -xml --fail-under=COV_FAIL_UNDER