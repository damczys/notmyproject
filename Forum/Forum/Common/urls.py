from django.urls import path
from django.conf.urls import url

from .views import indexView

app_name = "Common"
urlpatterns = [
    path('', indexView, name="index"),
]