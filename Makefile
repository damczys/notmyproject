# Makefile

SRC_DIR = Forum
SHELL :=/bin/bash
PY_VERSION = python3
PROJECT = NotMyProject

tests:
	cd $(SRC_DIR) && $(PY_VERSION) -m coverage run --branch manage.py test --debug-mode --debug-sql
	${PY_VERSION} -m coverage report -m

dev-env-up:
	docker-compose -p $(PROJECT) -f docker/docker-compose.yaml up -d --build --remove-orphans
	@while [ $$(docker ps --filter health=starting --format "{{.Status}}" | wc -l) != 0 ]; do echo 'waiting for healthy containers'; sleep 1; done

dev-env-down:
	docker-compose -p $(PROJECT) -f docker/docker-compose.yaml down -v --remove-orphans


dev-env-clean:
	echo "Clean"

